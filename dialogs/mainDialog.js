const { MessageFactory, InputHints, CardFactory } = require('botbuilder');
const { LuisRecognizer } = require('botbuilder-ai');
const { ComponentDialog, DialogSet, DialogTurnStatus, TextPrompt, WaterfallDialog } = require('botbuilder-dialogs');
const TopicCard = require('../resources/topics.json');
const YearCard = require('../resources/years.json');
const ContentCard = require('../resources/content.json');
const PaperCard = require('../resources/paper.json');
const QueCard1 = require('../resources/que1.json');
const QueCard2 = require('../resources/que2.json');
const WelcomeCard = require('../resources/welcome.json');


const MAIN_WATERFALL_DIALOG = 'mainWaterfallDialog';

class MainDialog extends ComponentDialog {
    constructor(luisRecognizer) {
        super('MainDialog');

        if (!luisRecognizer) throw new Error('[MainDialog]: Missing parameter \'luisRecognizer\' is required');
        this.luisRecognizer = luisRecognizer;

        // Define the main dialog and its related components.
        // This is a sample "book a flight" dialog.
        this.addDialog(new TextPrompt('TextPrompt'))
            .addDialog(new WaterfallDialog(MAIN_WATERFALL_DIALOG, [
                this.introStep.bind(this),
                this.actStep.bind(this)
            ]));

        this.initialDialogId = MAIN_WATERFALL_DIALOG;
    }

    /**
     * The run method handles the incoming activity (in the form of a TurnContext) and passes it through the dialog system.
     * If no dialog is active, it will start the default dialog.
     * @param {*} turnContext
     * @param {*} accessor
     */
    async run(turnContext, accessor) {
        const dialogSet = new DialogSet(accessor);
        dialogSet.add(this);

        const dialogContext = await dialogSet.createContext(turnContext);
        const results = await dialogContext.continueDialog();
        if (results.status === DialogTurnStatus.empty) {
            await dialogContext.beginDialog(this.id);
        }
    }

    /**
     * First step in the waterfall dialog. Prompts the user for a command.
     * Currently, this expects a booking request, like "book me a flight from Paris to Berlin on march 22"
     * Note that the sample LUIS model will only recognize Paris, Berlin, New York and London as airport cities.
     */
    async introStep(stepContext) {
        if (!this.luisRecognizer.isConfigured) {
            const messageText = 'NOTE: LUIS is not configured. To enable all capabilities, add `LuisAppId`, `LuisAPIKey` and `LuisAPIHostName` to the .env file.';
            await stepContext.context.sendActivity(messageText, null, InputHints.IgnoringInput);
            return await stepContext.next();
        }

        // const weekLaterDate = moment().add(7, 'days').format('MMMM D, YYYY');
        // const messageText = stepContext.options.restartMsg ? stepContext.options.restartMsg : `What can I help you with today?\nSay something like "Book a flight from Paris to Berlin on ${ weekLaterDate }"`;
        // const promptMessage = MessageFactory.text(messageText, messageText, InputHints.ExpectingInput);
        // return await stepContext.prompt('TextPrompt', { prompt: promptMessage });
        return await stepContext.next();
    }

    /**
     * Second step in the waterfall.  This will use LUIS to attempt to extract the origin, destination and travel dates.
     * Then, it hands off to the bookingDialog child dialog to collect any remaining details.
     */
    async actStep(stepContext) {
        
        // Call LUIS and gather any potential booking details. (Note the TurnContext has the response to the prompt)
        const luisResult = await this.luisRecognizer.executeLuisQuery(stepContext.context);
        console.log('LuisRecognizer.topIntent(luisResult)', LuisRecognizer.topIntent(luisResult));
        switch (LuisRecognizer.topIntent(luisResult)) {
        case `Are_you_looking_for_help_in_CEMC`: {
            const welcomeCard = CardFactory.adaptiveCard(WelcomeCard);
            await stepContext.context.sendActivity({ attachments: [welcomeCard] });
            break;
        }
        case `Can_you_name_the_topic_in_which_you're_interested`: {
            const topicCard = CardFactory.adaptiveCard(TopicCard);
            await stepContext.context.sendActivity({ attachments: [topicCard] });
            console.log('luisResult1--->', luisResult);
            break;
        }
        case 'Topic': {
            if(luisResult.text === 'Euclid' || luisResult.text === '"Euclid"'){
                const contentCard = CardFactory.adaptiveCard(ContentCard);
                await stepContext.context.sendActivity({ attachments: [contentCard] });    
            }
            else{
                const didntUnderstandMessageText = `Sorry, I didn't get that. Please try asking in a different way (intent was ${ LuisRecognizer.topIntent(luisResult) })`;
                await stepContext.context.sendActivity(didntUnderstandMessageText, didntUnderstandMessageText, InputHints.IgnoringInput);    
            }
            console.log('luisResult2--->', luisResult);
            break;
        }
        case 'Content': {
            if(luisResult.text === 'Question' || luisResult.text === '"Question"'){
                const yearCard = CardFactory.adaptiveCard(YearCard);
                await stepContext.context.sendActivity({ attachments: [yearCard] });    
            }
            else if(luisResult.text === 'Solution' || luisResult.text === '"Solution"'){
                const yearCard = CardFactory.adaptiveCard(YearCard);
                await stepContext.context.sendActivity({ attachments: [yearCard] });    
            }
            else{
                const didntUnderstandMessageText = `Sorry, I didn't get that. Please try asking in a different way (intent was ${ LuisRecognizer.topIntent(luisResult) })`;
                await stepContext.context.sendActivity(didntUnderstandMessageText, didntUnderstandMessageText, InputHints.IgnoringInput);    
            }
            console.log('luisResult content--->', luisResult);
            break;
        }
        case 'Year': {
            if(luisResult.text === '2021' || luisResult.text === '"2021"'){
                const paperCard = CardFactory.adaptiveCard(PaperCard);
                await stepContext.context.sendActivity({ attachments: [paperCard] });
            }    
            else{
                const didntUnderstandMessageText = `Sorry, I didn't get that. Please try asking in a different way (intent was ${ LuisRecognizer.topIntent(luisResult) })`;
                await stepContext.context.sendActivity(didntUnderstandMessageText, didntUnderstandMessageText, InputHints.IgnoringInput);    
            }        
            console.log('luisResult3--->', luisResult);
            break;
        }
        case 'Paper': {
            if(luisResult.text === 'Q1' || luisResult.text === '"Q1"'){
                const queCard1 = CardFactory.adaptiveCard(QueCard1);
                await stepContext.context.sendActivity({ attachments: [queCard1] });
            }    
            else if(luisResult.text === 'Q2' || luisResult.text === '"Q2"'){
                const queCard2 = CardFactory.adaptiveCard(QueCard2);
                await stepContext.context.sendActivity({ attachments: [queCard2] });
            }   
            else{
                const didntUnderstandMessageText = `Sorry, I didn't get that. Please try asking in a different way (intent was ${ LuisRecognizer.topIntent(luisResult) })`;
                await stepContext.context.sendActivity(didntUnderstandMessageText, didntUnderstandMessageText, InputHints.IgnoringInput);        
            }
            break;
        }
        case 'None': {
            break;
        }

        default: {
            // Catch all for unhandled intents
            const didntUnderstandMessageText = `Sorry, I didn't get that. Please try asking in a different way (intent was ${ LuisRecognizer.topIntent(luisResult) })`;
            await stepContext.context.sendActivity(didntUnderstandMessageText, didntUnderstandMessageText, InputHints.IgnoringInput);
        }
        }

        return await stepContext.next();
    }
}

module.exports.MainDialog = MainDialog;